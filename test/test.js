const { getCircleArea, checkIfPassed, getAverage, getSum, getDifference, div_check, isOddOrEven, reverseString } = require("../src/util.js");
//import assert statements from our chai
const { assert,expect } = require('chai');

//test case - a condition we are testing
//it(stringExplainsWhatTheTestDoes,functionToTest)
//assert is used to assert conditions for the test to pass. If the assertion fails, then the test is considered failed.
//describe() is used to create a test suite. A test suite is a group of test cases related to one another or tests the same method,data, or function.
describe('test_get_circle_area',()=>{
    
    it('test_area_of_circle_radius_15_is_706.86',()=>{
    
        let area = getCircleArea(15);
        assert.equal(area,706.86);
    
    });

    it('test_area_of_circle_radius_is_300_is_282744',()=>{

        let area = getCircleArea(300);
        expect(area).to.equal(282744);
    })

    
});

describe('test_check_if_passed',()=>{

    it('test_20_out_of_30_if_passed',()=>{

        let isPassed = checkIfPassed(25,30);
        assert.equal(isPassed,true);
    })

    it('test_30_out_of_50_is_not_passed',()=>{

        let isPassed = checkIfPassed(30,50);
        assert.equal(isPassed,false);

    })

});

describe('test_get_average',()=>{

    it('test_get_average_of_80_82_84_86_is_83',()=>{

        let average = getAverage(80,82,84,86);
        assert.equal(average,83);

    });

    it('test_get_average_of_70_80_82_84_is_79',()=>{

        let average = getAverage(70,80,82,84);
        assert.equal(average,79);

    });

});

describe('test_get_sum',()=>{

    it('test_15_plus_30_is_45',()=>{

        let sum = getSum(15,30);
        assert.equal(sum,45);

    });

    it('test_25_plus_50_is_75',()=>{

        let sum = getSum(25,50);
        assert.equal(sum,75);

    });
});

describe('test_get_difference',()=>{

    it('test_70_minus_40_is_30',()=>{

        let diff = getDifference(70,40);
        assert.equal(diff,30);

    });

    it('test_125_minus_50_is_75',()=>{

        let diff = getDifference(125,50);
        assert.equal(diff,75);

    });
});

describe('test_div_check',()=>{

    it('6_is_not_divisible_by_5_or_7',()=>{

        let check = div_check(6);
        assert.equal(check,false);
    });

    it('60_is_divisible_by_5_or_7',()=>{

        let check = div_check(60);
        assert.equal(check,true);
    });

    it('35_is_divisible_by_5_or_7',()=>{

        let check = div_check(35);
        assert.equal(check,true);
    });

    it('17_is_not_divisible_by_5_or_7',()=>{

        let check = div_check(6);
        assert.equal(check,false);
    });

});

describe('test_is_odd_or_even',()=>{

    it('46_is_even',()=>{

        let oddOrEven = isOddOrEven(46);
        assert.equal(oddOrEven,'even');

    });

    it('66_is_even',()=>{

        let oddOrEven = isOddOrEven(66);
        assert.equal(oddOrEven,'even');

    })

    it('47_is_odd',()=>{

        let oddOrEven = isOddOrEven(47);
        assert.equal(oddOrEven,'odd');

    })

    it('69_is_odd',()=>{

        let oddOrEven = isOddOrEven(69);
        assert.equal(oddOrEven,'odd');

    })

});

describe('test_reverse_string',()=>{

    it('the_reverse_of_hello_is_olleh',()=>{

        let revString = reverseString("hello");
        assert.equal(revString,"olleh");
    });

    it('the_reverse_of_bro_is_orb',()=>{

        let revString = reverseString("bro");
        assert.equal(revString,"orb");
    });
    
    it('the_reverse_of_pre_is_erp',()=>{

        let revString = reverseString("pre");
        assert.equal(revString,"erp");
    });
    
    it('the_reverse_of_joel_is_leoj',()=>{

        let revString = reverseString("joel");
        assert.equal(revString,"leoj");
    });
})

